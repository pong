all:
	${CC} -o pong pong.c ${CLAGS} -Wall -Wextra -std=c99 -pedantic \
		-lcurses -lm ${LDFLAGS}
	scdoc < pong.1.scd > pong.1

install: all
	mkdir -p /usr/local/bin /usr/local/share/man/man1
	cp pong /usr/local/bin/pong
	cp pong.1 /usr/local/share/man/man1/pong.1

uninstall:
	rm /usr/local/bin/pong /usr/local/share/man/man1/pong.1

.PHONY: all
